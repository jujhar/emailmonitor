# EmailMonitor

Configurable Java application to monitor specific folder of email and on receiving specific email as specified in configuration file, executes jar in separate thread. Name and path of jar can be configured in the configuration file.

# Executing application

*  First import this project and export as jar
*  Create a Folder and copy jar in it, and copy resources folder as well.
*  Navigate to resources/config folder and edit configuration.properties file as your need
*  Comments addded in configuration file to describe each variable